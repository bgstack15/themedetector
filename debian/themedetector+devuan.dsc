Format: 3.0 (quilt)
Source: themedetector
Binary: themedetector
Architecture: any
Version: 0.0.1-1+devuan
Maintainer: Ben Stack <bgstack15@gmail.com>
Homepage: https://bgstack15.wordpress.com/
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12), bgscripts-core (>= 1.5.0), libgtk-3-dev, txt2man
Package-List:
 themedetector deb x11 optional arch=any
Files:
 00000000000000000000000000000000 1 themedetector.orig.tar.gz
 00000000000000000000000000000000 1 themedetector+devuan.debian.tar.xz
