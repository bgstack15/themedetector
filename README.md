# README for themedetector
## Overview
This is a small utility that makes it easy to discover what the current default theme is for gtk3. Patches are welcome for providing support for other versions of gtk.

Implementations exist in C and in Python3. You will need gtk3 development headers to compile the C one.

## Purpose
Because [reading](https://bgstack15.wordpress.com/2019/07/21/set-gtk2-and-gtk3-themes-in-fluxbox/) `~/.gtkrc-2.0` or `~/.config/gtk-3.0/settings.ini` should be a oneliner!

## Additional thoughts
You should run these programs with stderr redirected to null (i.e., `2>/dev/null`) if your theme causes Gtk minor indigestion.

## References
### Weblinks
1. C version lifted entirely from [stackoverflow #54365212](https://stackoverflow.com/questions/54365212/gtk2-vs-gtk3-detect-gtk3-theme-in-bash-script/54384946#54384946)
