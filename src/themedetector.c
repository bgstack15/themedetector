// https://stackoverflow.com/questions/54365212/gtk2-vs-gtk3-detect-gtk3-theme-in-bash-script/54384946#54384946
#include <stdio.h>
#include <gtk/gtk.h>
int main() {
	gchar *prop;
   gtk_init(0, 0);
   g_object_get(gtk_settings_get_default(), "gtk-theme-name", &prop, 0);
   return !printf("%s\n", prop);
}
